# Hello World <span class="typcn typcn-world"></span>

from Benny

---

This is a simple website for storing my works, including some observations and findings. 

---

##### About Benny

First, I'm a geographer. I'm a Malaysian. I was born in Penang. When I was 9, I moved to Selangor with my family. I continued my study at Taipei after graduated from Pin Hwa High School, Klang. I finished my undergraduate and master degree in NTU, Taiwan. I worked as a research assistance before I continued studying as a PhD. student. 
Maybe moving around, meeting new people, and blending in different cultures, made me a geographer that like to explore the connections between places.  

Second, I'm a cartographer. I made maps most of the time ever since I was a graduate student. I made maps for text book, for summited articles authored by my advisor and friends, and two large maps (wall size) for the Geography Department, NTU. Those are static-printed map. In addition, I also make interactive-web map. I joined a research project which used web-map as the main tool for the early-warning system. I also created a medical resource accessibility map for participating a medical-map contest.  

Third, I'm a GIScientist. GIS is not just a mapping system. It is a system for gathering and presenting geographical information, and a platform for exploring the geographical-spaces. There are already so many analyzing methods for solving many types of problems. But, I believe this is just the beginning of the rising GIS. In the age of BIG DATA, with the help of computing power, there are still uncountable information await for digging from the geographical data. 

In simple words, I'm a guy who like thinking about connected spaces, working with computer, playing with data, drawing map, and writing some codes.

Therefore, I made this website to gather some ideas come into my head, to keep some notes, and hopefully to present some findings.

Thank you. 

**_Wei Chien Benny, Chin_**  
<span class="typcn typcn-mail"></span>: 
<a href="mailto:wcchin.88@gmail.com"><span class="typcn typcn-social-at-circular"></span> gmail </a> | 
<a href="mailto:wcchin88@yahoo.com"><span class="typcn typcn-social-at-circular"></span> yahoo </a> | 
<a href="mailto:benny-chin@sutd.edu.sg"><span class="typcn typcn-social-at-circular"></span> My SUTD email </a> .


