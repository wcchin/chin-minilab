
# Wei Chien Benny Chin  陳威全
(updated: 2019-09-16)

---

## Products of researches and projects
- Geographical PageRank (GPR) -  
  Description: a python package that provide functions of algorithms for measuring concentration distribution in a spatial network.  
  Project page: https://wcchin.github.io/geographical-pagerank-algorithms.html  
  Repository: https://bitbucket.org/wcchin/gpras  
  License: MIT license  

- Flow-based PageRank (FBPR) -  
  Description: a python package that run an algorithm that calibrate the weight and PR score to meet the flow.  
  Project page: https://wcchin.github.io/flow-based-pagerank.html  
  repository: https://bitbucket.org/wcchin/fbpr  
  License: MIT license  

- Taiwan Geographic Open Data (TGOD) -  
  Description: a python package that wrap some of the Taiwan open data real-time api, and some static map layers files inside the package, that can be called through some functions and get the data in dataframe  or geodataframe format.  
  Project page: https://wcchin.github.io/tgod/  
  Repository: https://github.com/wcchin/tgod  
  License: BSD-3-clause license.

- Vector MAP ProducER (vmapper) -  
  Description: a simple python library for creating SVG map in python.  
  Project page: https://wcchin.github.io/vmapper.html  
  Repository: https://github.com/wcchin/vmapper  
  License: MIT license  

- ColouringMap (colouringmap) -  
  Description: a convenient mapping tool for generating categories and colors for making choropleth map from geopandas gdf.  
  Project page: https://wcchin.github.io/colouringmap.html  
  Repository: https://github.com/wcchin/colouringmap  
  License: MIT license  

- TaPiTaS (tapitas) -  
  Description: a data exploration and visualization algorithm for understanding diffusion process..  
  Project page: https://bitbucket.org/wcchin/tapitas  
  Repository: https://bitbucket.org/wcchin/tapitas  
  License: MIT license  


## Other interesting and convenient tools 
- Carlae -  
  Description: a python package that generate single page website for github projects from a simple markdown file.  
  Project page: https://wcchin.github.io/carlae/   
  Repository: https://github.com/wcchin/carlae  
  License: MIT license  

- pyreveal

  Description: a python package that generate slides using markdown and reveal.js.  
  Project page: https://wcchin.github.io/pyreveal/index.html   
  Repository: https://github.com/wcchin/pyreveal  
  License: MIT license  

